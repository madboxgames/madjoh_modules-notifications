define([
	'require',
	'madjoh_modules/ajax/ajax',
	'madjoh_modules/session/session',
	'madjoh_modules/custom_events/custom_events',
	'madjoh_modules/cache/data_cache'
],
function(require, AJAX, Session, CustomEvents, DataCache){
	var Notification = {
		init : function(settings){
			Notification.settings = settings;

			CustomEvents.addCustomEventListener(document, 'LoggedIn', 	Notification.connect);
			CustomEvents.addCustomEventListener(document, 'LoggingOut', Notification.disconnect);
			document.addEventListener('deviceready', function(){
				document.addEventListener('resume', Notification.onResume, false);
			}, false);
		},

		connect : function(){
			var apiURL = AJAX.settings.getAPI();

			document.notificationSocket = io.connect(apiURL);
			document.notificationSocket.on('handshake', function(){
				var credentials = Session.getKeys();
					credentials.deviceId 	= document.deviceId;
					credentials.app_version = AJAX.settings.version;
				document.notificationSocket.emit('auth', credentials);	
			});
			document.notificationSocket.on('notification', function(data){
				Notification.settings.onNotification(data.notification);

				Notification.settings.clearCacheFromNotifications([data.notification]);
				Notification.getNew(function(notifications){
					Notification.getNewCallback(notifications);
					if(notifications.length > 0) Notification.vibrate();
				});
			});
			document.notificationSocket.on('info', function(data){
				console.log(data);
			});
			document.notificationSocket.on('auth_failure', function(){
				Session.close();
			});

			Notification.getNew(Notification.getNewCallback);
		},
		getNewCallback : function(notifications){
			DataCache.set('notifications.new', notifications);

			var types = Notification.settings.groupTypes(notifications);
			DataCache.set('notification.types', types);
			CustomEvents.fireCustomEvent(document, 'Notification', types);
			Notification.settings.clearCacheFromNotifications(notifications);
		},
		onResume : function(){
			if(!Session.isOpen()) return;
			
			Notification.getNew(function(notifications){
				Notification.getNewCallback(notifications);
			});
		},
		disconnect : function(){
			if(document.notificationSocket) document.notificationSocket.disconnect();
		},

		get : function(startIndex){
			var parameters = {};
			if(startIndex) parameters.startIndex = startIndex;

			return AJAX.post('/notification/get', {parameters : parameters}).then(function(result){
				DataCache.clear('notifications.new');
				return result.data.notifications;
			});
		},

		getNew : function(callback){
			AJAX.post('/notification/get/new').then(function(result){callback(result.data.notifications);});
		},
		vibrate : function(){
			if(window.cordova && navigator && navigator.vibrate) navigator.vibrate(500);
		},

		seeAll : function(){
			var needsAction = false;

			var cacheKey = 'notifications';
			var notifications = DataCache.get('notifications');
			if(!notifications){
				notifications = DataCache.get('notifications.new');
				cacheKey = 'notifications.new';
			}

			if(notifications){
				for(var i = 0; i < notifications.length; i++){
					if(notifications[i].status === 1){
						notifications[i].status = 2;
						needsAction = true;
					}
				}

				DataCache.set(cacheKey, notifications);
			}

			if(!needsAction){
				var notificationTypes = DataCache.get('notification.types');
				if(notificationTypes){
					if(notificationTypes.notifications > 0) needsAction = true;
				}
			}

			if(needsAction){
				var types = Notification.settings.groupTypes(notifications);
				DataCache.set('notification.types', types);
				CustomEvents.fireCustomEvent(document, 'Notification', types);

				return AJAX.post('/notification/see/all');
			}
		},
		treat : function(notification_id){
			var needsAction = false;
			var code 		= null;

			var cacheKey = 'notifications';
			var notifications = DataCache.get('notifications');
			if(!notifications){
				notifications = DataCache.get('notifications.new');
				cacheKey = 'notifications.new';
			}

			if(notifications){
				for(var i = 0; i < notifications.length; i++){
					if(notifications[i].notification_id === notification_id && notifications[i].status !== 3){
						notifications[i].status = 3;
						needsAction = true;
						code = notifications[i].code;
					}
				}

				DataCache.set(cacheKey, notifications);
			}

			if(needsAction){
				var types = Notification.settings.groupTypes(notifications);
				DataCache.set('notification.types', types);
				CustomEvents.fireCustomEvent(document, 'Notification', types);
				
				var parameters = {notification_id : notification_id};
				return AJAX.post('/notification/treat', {parameters : parameters});
			}
		},
		treatAll : function(){
			var needsAction = false;

			var cacheKey = 'notifications';
			var notifications = DataCache.get('notifications');
			if(!notifications){
				notifications = DataCache.get('notifications.new');
				cacheKey = 'notifications.new';
			}

			if(notifications){
				for(var i = 0; i < notifications.length; i++){
					if(notifications[i].status !== 3){
						notifications[i].status = 3;
						needsAction = true;
					}
				}

				DataCache.set(cacheKey, notifications);
			}

			if(!needsAction){
				var notificationTypes = DataCache.get('notification.types');
				if(notificationTypes){
					for(var key in notificationTypes){
						if(notificationTypes[key] > 0) needsAction = true;
					}
				}
			}
				
			if(needsAction){
				var types = Notification.settings.groupTypes(notifications);
				DataCache.set('notification.types', types);
				CustomEvents.fireCustomEvent(document, 'Notification', types);

				return AJAX.post('/notification/treat/all');
			}
		},
		treatType : function(code){
			var needsAction = false;

			var cacheKey = 'notifications';
			var notifications = DataCache.get('notifications');
			if(!notifications){
				notifications = DataCache.get('notifications.new');
				cacheKey = 'notifications.new';
			}

			if(notifications){
				for(var i = 0; i < notifications.length; i++){
					if(notifications[i].code === code && notifications[i].status !== 3){
						notifications[i].status = 3;
						needsAction = true;
					}
				}

				DataCache.set(cacheKey, notifications);
			}

			if(!needsAction){
				var notificationTypes = DataCache.get('notification.types');
				if(notificationTypes){
					var badgeKeys = Notification.settings.getTypeBadgeKeys(code);

					for(var i = 0; i < badgeKeys.length; i++){
						if(notificationTypes[badgeKeys[i]] > 0) needsAction = true;
					}
				}
			}

			if(needsAction){
				var types = Notification.settings.groupTypes(notifications);
				DataCache.set('notification.types', types);
				CustomEvents.fireCustomEvent(document, 'Notification', types);

				var parameters = {code : code};
				return AJAX.post('/notification/treat/type', {parameters : parameters});
			}
		},

		changeSubscription : function(disabled){
			var parameters = {disabled : JSON.stringify(disabled)};
			return AJAX.post('/notification/subscription', {parameters : parameters});
		}
	};

	return Notification;
});